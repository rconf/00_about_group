# About the rconf group

The rconf group is to manage repositories related to conferences organized/supported by the R Foundation.

This README gives an overview of groups and projects as of 15 October 2024.

## Groups

- R Conferences Group (https://gitlab.com/groups/rconf/-/group_members):
    - direct members: @hturner1 and @R-conferences (shared RFCC account)
    - group member: R Conference Committee Group
    - automatically added as members to any project within the group (or any subgroup)
    - have access to private repos (useRadmin and useRbids)

- R Conference Committee Group (https://gitlab.com/groups/r-conference-committee/-/group_members)   
    - direct members: people from R Foundation Conference Committee
    - Group member of R Conferences Group
    - Automatically added as member to any project set up by rconf.

- useR Assets Group (https://gitlab.com/groups/rconf/user-assets/-/group_members)
    - subgroup of rconf
    - shared projects: useRinfoboard, useRknowledgebase, useRorganization, useR-2021-global (project under user-2021-team group)
    - direct members: technical writers and steering committee from GSoD 2021 project, other useR! organizers helping to maintain these assets.

### Repos

R Conferences Group and R Conference Committee Group are members (owners) of all repos. 

#### Active

 - Code of Conduct repo
    - direct members: people from useR! Working Group and code of conduct response team

 - useR! 2022 website
    - direct members: people from useR! 2022 organizing team

 - useR! 2024 website
    - direct members: people from useR! 2024 organizing team

 - useR! 2025 website
    - direct members: people from useR! 2025 organizing team

 - useR global working group  
    - direct members: people from useR! Working Group

 - useR Tech Note Template 
    - direct members: people from useR! 2021 organizing team

 - useR xaringan theme 
    - direct members: people from useR! 2021 organizing team

 - useR2020 (main website repo)
    - direct members: people from useR! 2020 organizing team and website template developers

 - useR2020muc (satellite website repo)
    - direct members: people from useR! 2020 satellite organizing team

 - useR2021 (website repo)
    - direct members: people from useR! 2021 organizing team

 - useRadmin
    - direct members: people from useR! Working Group and main organizers from useR! conferences

 - useRbids
    - direct members: people from R Foundation Conference Committee

 - useRinfoboard
    - group member: useR Assets
     
 -  useRknowledgebase
    - group member: useR Assets
    - direct members: people from useR! Working Group

 - useRorganization
    - group members: useR Assets, useR 2021 team/organizers (all organizers), useR 2021 team (co-ordinators)
    - direct members: people from useR! 2020 team, others from 2021 team and useR Assets

 - useR template (xaringan template from 2020)
    - direct members: contributor from useR! 2020 team

#### Inactive (archived)

Archived repos can be viewed on the Inactive tab of <https://gitlab.com/rconf>.

 - Community Events 2023

 - useR Hugo Theme

 - useR Website Template
